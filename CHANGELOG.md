# Change Log

## 5.5.2 (2021-09-29)
- Permettre d'ajouter un badge dans le menu de navigation. (#17)
- Ajout d'icônes de « dolly » et de « tally-marks ».

## 5.5.1 (2021-03-12)
- Ne pas afficher le journal des évènements. (#15)

## 5.5.0 (2021-03-11)
- Permettre le changement staging/production directement dans l'application. (#14)

## 5.4.0 (2019-02-09)
- Ajout de la librarie stacktrace-js. (#13)

## 5.3.0 (2019-01-11)
- Ajout d'une zone dans le footer pour afficher des notifications. (#12)

## 5.2.0 (2018-12-21)
- Ajout d'icônes play et pause. (#11)

## 5.1.0 (2018-05-10)
- Créer un service de gestion des boites de dialogues (#6)

## 5.0.0 (2018-05-10)
- Mise à jour des icônes octicons. (#7)
- Les icônes ont été séparées dans deux dossier différents: `festo` et `octicons` pour faciliter les futures mises à jour d'icônes. 

## 4.0.0 (2018-04-19)
- Création d'un filtre pour convertir les titres en Anglais. (#5)
- Inclure les librairies angular-xeditable et angular-tooltips.

## 3.0.0 (2018-04-04)
- Les icônes affichent correctement sous IE. (#4)

## 2.0.0 (2018-04-03)
- Les icônes dans le menu de gauche peuvent être stylées.

## 1.3.0 (2018-03-29)
- Ajout des icônes pour: Routing, Suspens, Historique de production

## 1.0.0 (2018-03-06)
- Initialisation de l'application.
