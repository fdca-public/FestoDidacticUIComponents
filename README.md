# UTILISER FestoDidacticUIComponents
1. Dans votre project, utiliser bower pour télécharger le projet (bower install https://festo-didactic.githost.io/fdca-public/FestoDidacticUIComponents.git --save)
2. Dans le fichier index.html, ajouter le logo FESTO, les sources Javascript et les sources CSS :

`<link rel="icon" href="<asset_directory>/FestoDidacticUIComponents/resources/images/favicon.ico" sizes="16x16" type="image/png">`

`<script src="<asset_directory>/FestoDidacticUIComponents/dist/js/festo-didactic-ui-components.bundle.min.js"></script>`

`<link rel="stylesheet" href="<asset_directory>/FestoDidacticUIComponents/dist/css/festo-didactic-ui-components.css">`


# NOUVELLE VERSION
## Compiler les fichiers js et css

Si des changements dans les fichiers scss ou js/html sont fait, il faut re-créer les fichiers minifié avec gulp :

Avec un maven install le dossier dist est regénéré, alors pas besoin d'y aller manuellement avec gulp.

On peut le faire manuellement en exécutant les commandes gulp: 
1. Pour les fichiers scss, executer : `gulp css-bundle` ce qui devrait re-générer le fichier dist\css\festo-didactic-ui-components.css
2. Pour les fichiers html/js (les htmls seront incorporé dans le fichier js final) executer : `gulp js-bundle` ce qui devrait re-générer le fichier dist\js\festo-didactic-ui-components.bundle.min.js

## Tester en local

Pour tester en local, on doit faire maven install et ensuite pousser les fichiers festo-didactic-ui-components.bundle.min.js et festo-didactic-ui-components.bundle.css sur le serveur git.
Ensuite dans le bower de l'application qui utilise FestoDidacticUIComponent, ajouter le numéro de commit au lieu du numéro de version ex : https://gitlab.com/fdca-public/FestoDidacticUIComponents.git#30c06727.

# COMPONENTS
## Menu Principal
```html
<fd-header config="myConfig"></fd-header>
```

### Configuration
```javascript
{
	brand: {
		location: "./resources/images/festo_logo_header.png", //Location du logo FESTO
		link: "#" // Lien lorsque du click sur le logo
	},
	navigation : [
		{
			name : "Materials", 												//Nom affiché dans le menu
			sref : "material.list", 											//View à charger (voir ui.router)
			isActive : function() {  											//Condition pour considerer l'élément comme actif
				return $state.includes('material');
			},
			subNavigation : {
				isVisible : function() {
					return $state.includes('material.detail');                  //Condition pour afficher le sous-menu
				},
				navItems : [
					{
						name : "Bill Of Materials",
						sref : "material.detail.billofmaterials.list",
						isActive : function() {
							return $state.includes('material.detail.billofmaterials');
						},
						icon : { 
							source: "<PATH_SVG>/list-unordered.svg",                               // path du fichier svg
							class: {class: true, otherClass: false},
							style: {"property": "value", "otherProperty": "value"}
						},
						subItems : [
							{
								name : "List",
								sref : "material.detail.billofmaterials.list",
								isActive : function() {
									return $state.is('material.detail.billofmaterials.list');
								}
							}
						]
					}
				]
			}
		},
		{
			...
		}
	]
}
```

## Menu Lateral (Sous-menu)
```html
<fd-sidebar config="subMenuConfig"></fd-header>
```

### Configuration
Doit correspondre à un sous-menu de myConfig.navigation (ex : myConfig.navigation[0].subNavigation.navItems)

## Pagination
Utiliser les directives `dir-paginate` et `dir-pagination-controls` comme ici :

```html
<tbody>
	<tr dir-paginate="searchResult in searchResults | itemsPerPage:15">
		<td><a ui-sref="material.detail.overview({materialNumber : searchResult.materialNumber})">{{ searchResult.materialNumber }}</a></td>
		<td>{{ labVoltPartNumberService.stringifyLabVoltPartNumber(searchResult.labVoltPartNumber) }}</td>
		<td>{{ searchResult.description }}</td>
	</tr>
</tbody>
<tfoot>
	<tr>
		<td colspan="3" style="padding:0">
			<dir-pagination-controls max-size="11" direction-links="true" boundary-links="true"></dir-pagination-controls>
		</td>
	</tr>
</tfoot>
```

## Footer
```html
<fd-footer config="footerConfig"></fd-footer>
```

### Configuration
```javascript
footer: {
	mode: "DEVELOPMENT",                                // Mode qui sera affiché en bas à droite de l'écran (TEST, STAGING, ...)
	notifications: {
		icon: "./resources/lib/FestoDidacticUIComponents/resources/icons/svg/bell.svg"
	}
}
```

Le footer affiche lui-même le numéro de version à partir du CHANGELOG ainsi que le CHANGELOG lui-même.
Pour cela, il faut rajouter le modal suivant dans le index.html :

```html
<body>
 ...
	<div id="changelog-modal" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div btf-markdown="changelog"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
  ...
</body>
```

On s'attend à ce que le changelog soit dans `<PROJECT_NAME>\src\main\webapp\changelog\CHANGELOG.md`.
Ajouter le plugin suivant dans le `pom.xml` pour copier automatiquement le changelog dans le bon repertoire :

```xml
<plugin>
	<artifactId>maven-resources-plugin</artifactId>
	<version>2.7</version>
	<executions>
		<execution>
			<id>copy-resource-changelog</id>
			<phase>prepare-package</phase>
			<goals>
				<goal>copy-resources</goal>
			</goals>
			<configuration>
				<outputDirectory>${project.build.directory}/${project.artifactId}-${project.version}/resources/changelog</outputDirectory>
				<resources>
					<resource>
						<directory>${basedir}</directory>
						<includes>
							<include>CHANGELOG.md</include>
						</includes>
					</resource>
				</resources>
			</configuration>
		</execution>
	</executions>
</plugin>
```