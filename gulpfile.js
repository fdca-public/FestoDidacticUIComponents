var gulp = require('gulp'),
    concat = require('gulp-concat'),
    templateManager = require('gulp-angular-template-manager'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    exec = require('child_process').exec;

gulp.task('js-components', function () {
    return gulp.src(['src/js/**/*.js'])
        .pipe(concat('festo-didactic-ui-components.js'))
        .pipe(templateManager({embed_all: true}))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js-dependencies', function () {
    return gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/jquery-ui/jquery-ui.min.js',
        'bower_components/moment/min/moment-with-locales.min.js',
        'bower_components/angular/angular.min.js',
        'bower_components/angular-ui-router/release/angular-ui-router.min.js',
        'bower_components/angular-translate/angular-translate.min.js',
        'bower_components/angular-sanitize/angular-sanitize.min.js',
        'bower_components/angular-cookies/angular-cookies.min.js',
        'bower_components/angular-resource/angular-resource.min.js',
        'bower_components/angular-animate/angular-animate.min.js',
        'bower_components/angular-flash-alert/dist/angular-flash.min.js',
        'bower_components/showdown/src/showdown.js',
        'bower_components/angular-markdown-directive/markdown.js',
        'bower_components/angular-utils-pagination/dirPagination.js',
        'bower_components/bootstrap/dist/js/bootstrap.bundle.min.js',
        'bower_components/x2js/xml2json.min.js',
        'bower_components/angular-tooltips/dist/angular-tooltips.min.js',
        'bower_components/angular-xeditable/dist/js/xeditable.min.js',
        'bower_components/stacktrace-js/dist/stacktrace.min.js'
    ])
        .pipe(concat('festo-didactic-ui-components-dependencies.min.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('sass-compile', function (cb) {
    return gulp.src('src/scss/bootstrap.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename("festo-didactic-ui-components.css"))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('js-bundle', gulp.series('js-components', 'js-dependencies', function () {
    return gulp.src([
        'dist/js/festo-didactic-ui-components-dependencies.min.js',
        'dist/js/festo-didactic-ui-components.js'
    ])
        .pipe(concat('festo-didactic-ui-components.bundle.min.js'))
        .pipe(gulp.dest('dist/js'));
}));

gulp.task('css-bundle', gulp.series('sass-compile', function () {
    return gulp.src([
        'dist/css/festo-didactic-ui-components.css',
        'bower_components/angular-tooltips/dist/angular-tooltips.min.css',
        'bower_components/angular-xeditable/dist/css/xeditable.min.css'
    ])
        .pipe(concat("festo-didactic-ui-components.bundle.css"))
        .pipe(gulp.dest('dist/css'));
}));
