(function () {
	'use strict';

	angular
		.module('festoDidacticUIComponents')
		.component('fdFooter', {
			bindings: {
				config: '<'
			},
			templateUrl: 'components/footer/footer.template.html',

			controller: function ($rootScope, $scope, changelogService, eventLogService) {
				var ctrl = this;

				$scope.selectedLogEvent = null;

				$scope.onClickLogEvent = function (event) {
					$scope.selectedLogEvent = event;

					$('#fdui-event-log-modal').modal();
				};

				$scope.onClickCloseLogEventDetail = function () {
					$scope.selectedLogEvent = null;

					$("#fdui-event-log-modal").modal('hide');
				};

				$scope.getBadge = function () {
					let unreadEvents = eventLogService.getUnreadEvents();

					if (unreadEvents.length === 0)
						return {className: 'badge-secondary', count: 0};

					let className = 'badge-success';
					for (let i = 0; i < unreadEvents.length; i++) {
						if (unreadEvents[i].severity === 'warning') {
							className = 'badge-warning';
						} else if (unreadEvents[i].severity === 'danger') {
							className = 'badge-danger';

							break;
						}
					}

					return {className: className, count: unreadEvents.length};
				};

				$scope.getEvents = function () {
					return eventLogService.events;
				};

				$scope.toggleEventLogDetails = function () {
					let $eventLogDetails = $('#fdui-event-log-details');

					if ($eventLogDetails.is(':visible')) {
						eventLogService.markAsRead();

						$eventLogDetails.hide();
					} else {
						$eventLogDetails.show();
						$eventLogDetails.find('#fdui-event-log-details-list').scrollTop(function () {
							return this.scrollHeight;
						});
					}
				};

				$scope.clearEventLog = function () {
					eventLogService.clear();

					$('#fdui-event-log-details').hide();
				};

				changelogService.get().then(
					function successCallback(response) {
						$rootScope.changelog = response.data;
						ctrl.version = $rootScope.changelog.match(/## \d+\.\d+\.\d+/g)[0].replace("## ", "");
					},
					function errorCallBack(error) {
						console.log(error);
					}
				);

				$('[data-toggle="popover"]').popover();

				ctrl.showChangelog = function () {
					$('#changelog-modal').modal();
				};

				ctrl.showStagingDialog = function () {
					let stagingModal = $("#staging-modal");
					$("body").append(stagingModal.detach());
					stagingModal.modal('show');
				};

				ctrl.onClickRedirectToStagingApplication = function () {
					$rootScope.elements.footer.stagingStatus.enableStaging();
				};

				ctrl.onClickRedirectToProductionApplication = function () {
					$rootScope.elements.footer.stagingStatus.disableStaging();
				};
			}
		});
})();