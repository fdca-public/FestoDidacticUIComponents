(function () {
    'use strict';

    angular
        .module('festoDidacticUIComponents')
        .component('fdHeader', {
            bindings: {
                config: '<'
            },
            templateUrl: 'components/header/header.template.html',
            controller: function ($state) {
                this.$state = $state;
            }
        });
})();