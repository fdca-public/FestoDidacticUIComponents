(function () {
    'use strict';

    angular
        .module('festoDidacticUIComponents')
        .component('fdSidebar', {
            bindings: {
                config: '<'
            },
            templateUrl: 'components/sidebar/sidebar.template.html',
            controller: function () {
                var $ctrl = this;
            }
        });
})();