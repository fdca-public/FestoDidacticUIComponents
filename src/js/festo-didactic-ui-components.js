(function () {
	'use strict';

	angular
		.module('festoDidacticUIComponents', [
			'ui.router',
			'angularUtils.directives.dirPagination',
			'btford.markdown',
			'pascalprecht.translate'
		])
		.config(function ($translateProvider) {
			$translateProvider.translations('en', {
				FDUI_FOOTER_EVENT_LOG: 'Event Log',
				FDUI_FOOTER_EVENT_LOG_CLEAR: 'Clear',
				FDUI_FOOTER_EVENT_DETAIL_CLOSE: 'Close',
				FDUI_FOOTER_EVENT_LOG_DATETIME: 'Date',
				FDUI_FOOTER_EVENT_LOG_TITLE: 'Title',
				FDUI_FOOTER_EVENT_LOG_SEVERITY: 'Severity',
				FDUI_FOOTER_EVENT_LOG_DETAIL: 'Detail',
				FDUI_FOOTER_NEW_FEATURE: 'Psst! New features are now available!',
				FDUI_FOOTER_FEEDBACK_FEATURE: 'What do you think about the new features?',
				FDUI_FOOTER_STAGING_NEW_FUNCTIONALITY: 'New features have been developed to help you be more efficient!',
				FDUI_FOOTER_STAGING_BEFORE_ACTIVATING: 'But before activating them automatically to all your colleagues, we need your help to make sure they meet the need and work properly.',
				FDUI_FOOTER_STAGING_HERE_NEW_FEATURES: 'Here are the new feature(s):',
				FDUI_FOOTER_STAGING_TROUBLE_WITH_FEATURES: 'These new features do not behave as you would like? Are they causing you problems?',
				FDUI_FOOTER_STAGING_BEFORE_DISABLING: 'Before disabling them, please take the time to explain to us why they don\'t work for you by creating an IT service request.',
				FDUI_FOOTER_STAGING_APOLOGIZE: 'We deeply apologize for taking a wrong path. We will do everything we can to prevent it from happening again.',
				FDUI_FOOTER_STAGING_DISABLE_FEATURES: 'Disable new features',
				FDUI_FOOTER_STAGING_TRY_FEATURES:'Try these new features',
				FDUI_FOOTER_STAGING_TELL_US_WHY: 'Tell us why by filling out an IT service request! It\'s important that we know why so we can make the necessary corrections quickly.',
				FDUI_FOOTER_STAGING_PREVENTING_DOING_JOB:'Are they preventing you from doing your job properly? In this case, it is understandable that you want to deactivate them.',
				FDUI_FOOTER_STAGING_GO_AHEAD_TRY_THEM_OUT:'Go ahead, try them out!'
			});

			$translateProvider.translations('fr', {
				FDUI_FOOTER_EVENT_LOG: 'Journal des évènements',
				FDUI_FOOTER_EVENT_LOG_CLEAR: 'Vider',
				FDUI_FOOTER_EVENT_DETAIL_CLOSE: 'Fermer',
				FDUI_FOOTER_EVENT_LOG_DATETIME: 'Date',
				FDUI_FOOTER_EVENT_LOG_TITLE: 'Titre',
				FDUI_FOOTER_EVENT_LOG_SEVERITY: 'Gravité',
				FDUI_FOOTER_NEW_FEATURE: 'Psst! De nouvelles fonctionnalités vous attendent!',
				FDUI_FOOTER_FEEDBACK_FEATURE: 'Que pensez-vous des nouvelles fonctionnalités?',
				FDUI_FOOTER_STAGING_NEW_FUNCTIONALITY: 'De nouvelles fonctionnalité ont été développées pour vous aider à être plus efficace!',
				FDUI_FOOTER_STAGING_BEFORE_ACTIVATING: 'Mais avant de les activer automatiquement à tous vos collègues, on a besoin de votre aide pour s\'assurer qu\'elles répondent bien au besoin et qu\'elles fonctionnent correctement.',
				FDUI_FOOTER_STAGING_HERE_NEW_FEATURES: 'Voici la/les nouvelles fonctionnalités :',
				FDUI_FOOTER_STAGING_TROUBLE_WITH_FEATURES: 'Ces nouvelles fonctionnalités ne se comportent pas comme vous l\'auriez souhaité? Elles vous causent des soucis?',
				FDUI_FOOTER_STAGING_BEFORE_DISABLING: 'Mais avant de les désactiver, prenez le temps de nous expliquer pourquoi elles ne fonctionnent pas pour vous en créant une demande informatique.',
				FDUI_FOOTER_STAGING_APOLOGIZE: 'On s\'excuse profondément d\'avoir pris un mauvais chemin. On mettra tout en oeuvre pour éviter que ça se reproduise.',
				FDUI_FOOTER_STAGING_DISABLE_FEATURES: 'Désactiver les nouvelles fonctionnalités',
				FDUI_FOOTER_STAGING_TRY_FEATURES:'Essayer ces nouvelles fonctionnalités',
				FDUI_FOOTER_STAGING_TELL_US_WHY: 'Dites-nous pourquoi en remplissant une demande informatique! C\'est important qu\'on le sache afin qu\'on puisse corriger le tir rapidement.',
				FDUI_FOOTER_STAGING_PREVENTING_DOING_JOB:'Elles vous empêchent de faire votre travail convenablement? Dans ce cas, on comprend que vous vouliez les désactiver.',
				FDUI_FOOTER_STAGING_GO_AHEAD_TRY_THEM_OUT:'Allez-y, essayez-les!'
			});
		});
})
();

