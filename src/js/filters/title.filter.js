(function () {
	'use strict';

	angular
		.module('festoDidacticUIComponents')
		.filter('title', function ($translate) {
			return function (title) {
				if ($translate.proposedLanguage() !== 'en')
					return title;

				// https://stackoverflow.com/a/4878800/1362049
				return title.replace(/\b\w+/g, function(txt) {
					return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
				});
			}
		});
})();