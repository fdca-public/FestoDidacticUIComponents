(function () {
    'use strict';

    angular
        .module('festoDidacticUIComponents')
        .factory('changelogService', changelogService);

    changelogService.$inject = ['$http'];

    function changelogService($http) {
        const _get = function () {
            return $http.get('./resources/changelog/CHANGELOG.md');
        };

        return {
            get: _get
        };
    }
})();
