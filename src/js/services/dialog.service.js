(function () {
    'use strict';

    angular
        .module('festoDidacticUIComponents')
        .factory('dialogService', dialogService)
        .constant('modalTemplate',
            '<div class="modal" tabindex="-1" role="dialog">' +
            '<div class="modal-dialog modal-dialog-centered" role="document">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<h5 class="modal-title">{{modalTitle}}</h5>' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button>' +
            '</div>' +
            '<div class="modal-body">' +
            '{{modalBody}}' +
            '</div>' +
            '<div class="modal-footer">' +
            '{{modalFooter}}' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

    function dialogService($interpolate, modalTemplate) {
        const _show = function (params) {

            let guid = "MyModal";

            $("body").append($(params.template).attr('id', guid));

            if (params.size === 'lg')
                $('#' + guid + " .modal-dialog").addClass("modal-lg");
            else if (params.size === 'sm')
                $('#' + guid + " .modal-dialog").addClass("modal-sm");

            $('#' + guid + " .modal-header").addClass("bg-" + params.background);

            if (['primary', 'success', 'danger'].indexOf(params.background) !== -1)
                $('#' + guid + " .modal-header").addClass("text-white");


            $('#' + guid).modal();

            $('#' + guid).on('hidden.bs.modal', function (e) {
                $('#' + guid).remove();
            });
        };

        const _showAlert = function (params) {

            params.footer = params.footer || '<button type="button" class="btn btn-' + params.background + '" data-dismiss="modal">OK</button>';

            _show({
                template: $interpolate(modalTemplate)({"modalTitle": params.title, "modalBody": params.body, "modalFooter": params.footer}),
                size: params.size || 'sm',
                background: params.background || "default"
            });
        };

        const _showSuccessAlert = function (params) {
            params.background = 'success';
            _showAlert(params);
        };

        const _showDangerAlert = function (params) {
            params.background = 'danger';
            _showAlert(params);
        };

        const _showWarningAlert = function (params) {
            params.background = 'warning';
            _showAlert(params);
        };

        const _showInfoAlert = function (params) {
            params.background = 'info';
            _showAlert(params);
        };


        return {
            show: _show,
            showAlert: _showAlert,
            showSuccessAlert: _showSuccessAlert,
            showDangerAlert: _showDangerAlert,
            showWarningAlert: _showWarningAlert,
            showInfoAlert: _showInfoAlert,
        };
    }
})();
