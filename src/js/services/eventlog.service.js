(function () {
	'use strict';

	angular
		.module('festoDidacticUIComponents')
		.factory('eventLogService', eventLogService);

	eventLogService.$inject = [];

	function eventLogService() {
		let events = [];

		const addSuccessEvent = function (title, detail) {
			addEvent(title, detail, 'success');
		};

		const addWarningEvent = function (title, detail) {
			addEvent(title, detail, 'warning');
		};

		const addDangerEvent = function (title, detail) {
			addEvent(title, detail, 'danger');
		};

		const addEvent = function (title, detail, severity) {
			events.push({
				datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
				title: title,
				detail: detail,
				severity: severity,
				read: false
			})
		};

		const getUnreadEvents = function () {
			return events.filter(function (event) {
				return !event.read
			});
		};

		const markAsRead = function () {
			getUnreadEvents().forEach(function (event) {
				event.read = true;
			})
		};

		const clear = function () {
			events.length = 0; // https://stackoverflow.com/a/1232046/1362049
		};

		return {
			events: events,

			addSuccessEvent: addSuccessEvent,
			addWarningEvent: addWarningEvent,
			addDangerEvent: addDangerEvent,
			addEvent: addEvent,

			getUnreadEvents: getUnreadEvents,

			markAsRead: markAsRead,
			clear: clear
		};
	}
})();
